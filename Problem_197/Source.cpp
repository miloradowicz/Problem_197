#include <algorithm>
#include <limits>
#include <map>
#include <vector>
#include <iostream>

struct triad;

using ids_t = std::map<std::string, int>;
using net_t = std::vector<std::vector<triad>>;
using memo_t = std::vector<std::vector<int>>;
using pair_t = std::pair<int, int>;

const int INF = std::numeric_limits<int>::max() - 1000;

struct triad {
  int _id;
  int _time;
  int _cost;
};

pair_t dp_approach(const net_t &net, int budget, int orig, int dest) {
  int N = net.size();
  memo_t memo(N, std::vector<int>(budget + 1, INF));
  memo[orig][0] = 0;

  for (int c = 0; c <= budget; c++) {
    for (int i = 0, a = orig; i < N; i++, a = (++a) % N) {
      for (const auto &e : net[a]) {
        int c2 = c + e._cost;
        if (c2 <= budget)
          memo[e._id][c2] = std::min(memo[e._id][c2], memo[a][c] + e._time);
      }
    }
  }

  pair_t r;
  auto begin_it = memo[dest].begin(), end_it = memo[dest].end();
  auto it = std::min_element(begin_it, end_it);

  return pair_t(*it, *it == INF ? INF : it - begin_it);
}

int main() {
  ids_t ids;
  net_t net;

  int N, M, S;
  std::cin >> N >> M >> S;
  net.resize(N);
  std::string pt, pt2;
  for (int i = 0; i < N; i++) {
    std::cin >> pt;
    ids.insert({ pt, i });
  }
  int t, c;
  for (int i = 0; i < M; i++) {
    std::cin >> pt >> pt2 >> t >> c;
    net[ids[pt]].push_back(triad{ ids[pt2], t, c });
    net[ids[pt2]].push_back(triad{ ids[pt], t, c });
  }
  int orig, dest;
  std::cin >> pt >> pt2;
  orig = ids[pt];
  dest = ids[pt2];

  pair_t p = dp_approach(net, S, orig, dest);
  if (p.first != INF && p.second != INF)
    std::cout << p.first << ' ' << p.second;
  else
    std::cout << "No solution";

  return 0;
}